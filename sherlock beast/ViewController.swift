//
//  ViewController.swift
//  sherlock beast
//
//  Created by Aina Jain on 25/01/17.

import UIKit

//Enum for determining input case
enum InputCase
{
    case range,
    array
}

class ViewController: UIViewController,UITextFieldDelegate
{
    //Local Variables
    var inputCase : InputCase = InputCase.range
    var str = ""
    var inputArray = [NSInteger]()
    //IBOutlets
    @IBOutlet weak var arrayElements: UILabel!
    @IBOutlet weak var addToArrayButton: UIButton!
    @IBOutlet weak var decentNumber: UITextField!
    @IBOutlet weak var getNumberButton: UIButton!
    @IBOutlet weak var inputTextField: UITextField!
    
    //Application Lifecycle Methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        addToArrayButton.isHidden = true
        getNumberButton.isHidden = true
        arrayElements.isHidden = true
        inputTextField.delegate = self
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    //IBOutlet Methods
    
    //Action handling for Segment Controller
    @IBAction func changeSegment(_ sender: UISegmentedControl)
    {
        inputTextField.resignFirstResponder()
        decentNumber.text = ""
        inputTextField.text = ""
        if sender.selectedSegmentIndex == 0
        {
            addToArrayButton.isHidden = true
            getNumberButton.isHidden = true
            arrayElements.isHidden = true
            inputArray.removeAll()
        }
        else
        {
            addToArrayButton.isHidden = false
            getNumberButton.isHidden = false
            arrayElements.isHidden = false
        }
    }
    
    //Action handling for addToArrayButton
    @IBAction func addToArray(_ sender: UIButton)
    {
        inputArray.append(Int(inputTextField.text!)!)
        var arrayChars = ""
        for i in inputArray
        {
            arrayChars = arrayChars.appendingFormat("%@,", String(i))
            arrayElements.text = arrayChars
        }
        inputTextField.text = ""
    }
    
    //Action handling for getNumberButton
    @IBAction func getDecentNumberAmongArray(_ sender: UIButton)
    {
        inputCase = InputCase.array
        getDecentNumber(range: nil,array: inputArray)
    }
    
    //TextFieldDelegate Method
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        resignFirstResponder()
        str = ""
        inputCase = InputCase.range
        getDecentNumber(range: Int(inputTextField.text!)!,array: nil)
        return true
    }
    
    //Will get the Biggest Possible Decent Number
    func getDecentNumber(range:NSInteger?, array:[NSInteger]?)
    {
        switch inputCase
        {
        case .range:
            
            if let range = range
            {
                if range == 1 || range == 2
                {
                    decentNumber.text = "no such case exist"
                    return
                }
                for value in 3...range
                {
                    str = ""
                    let c = 5*((2*value)%3)
                    
                    if(c>value)
                    {
                        print("-1")
                    }
                    else
                    {
                        let i = value - c
                        for _ in 0..<i
                        {
                            str = str + String("5")
                        }
                        for _ in 0..<c
                        {
                            str = str + String("3")
                        }
                        
                        decentNumber.text = str
                        print(decentNumber.text! as String)
                    }
                }
            }
        case .array:
            if let array = array
            {
                for value in array
                {
                    str = ""
                    let c = 5*((2*value)%3)
                    
                    if(c>value)
                    {
                        print("-1")
                    }
                    else
                    {
                        let i = value - c
                        for _ in 0..<i
                        {
                            str = str + String("5")
                        }
                        for _ in 0..<c
                        {
                            str = str + String("3")
                        }
                        decentNumber.text = str
                        print(decentNumber.text! as String)
                    }
                }
            }
        }
        
    }
}



